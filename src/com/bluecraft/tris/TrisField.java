//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Tris: The Addictive Game! in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Tris: The Addictive Game!
Tris is a Tetr*s-clone game.
Each brick in Tris consists of three triangular pieces.

@copyright  Hyoungsoo Yoon
@date  April 25th, 1999
*/
package com.bluecraft.tris;

import com.bluecraft.tris.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


/**
TrisField is ...

@author  Hyoungsoo Yoon
@version  0.3
*/
public class TrisField extends JPanel implements TrisSquare {
  
    final private static int nWidth = 10;
    final private static int nHeight = 21;
    private int fieldWidth = nWidth*unit;
    private int fieldHeight = nHeight*unit;

    private int rotDirection = 1; // 1: CW, 2: CCW 
    private int interval = 500; // 500 ms, interval between steps
    private int gameState = 0; // 0: new, 1: in Pause, 2: game over

    private int[][] field;  // 0: none, 1: NW, 2: NE, 4: SE, 8: SW,
    // 5: NW+SE, 10: NE+SW


    private int currentBrick;
    private int currentPos;
    private int pileHeight;
    private int nextBrick;

    private int rowDeleted = 0;
    private int pileDensity = 0;
    private int score = 0;


    public TrisField() {

        field = new int[nWidth][nHeight];


	//
        field[1][1] = 1;
        field[2][2] = 2;
        field[3][3] = 4;
        field[3][4] = 5;
        field[3][6] = 8;
        field[3][7] = 10;
	//

        try  {
            // Lay out the game-field pane.
            setLayout(new BorderLayout());
            setPreferredSize(new Dimension(fieldWidth, fieldHeight));
            setMinimumSize(new Dimension(fieldWidth, fieldHeight));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        //g.clearRect(0,0,fieldWidth,fieldHeight);
        g.setColor(Color.gray);
        g.fillRect(0,0,fieldWidth,fieldHeight);

        for(int i=0;i<nWidth;i++) {
            for(int j=0;j<nHeight;j++) {
                // Empty squares
                g.setColor(trisColor[0]);
                trisPiece[0].translate(i*unit,j*unit);
                g.fillPolygon(trisPiece[0]);
                trisPiece[0].translate(-i*unit,-j*unit);

                for(int k=1;k<=4;k++) {
                    if((field[i][j] & trisMask[k]) != 0) {
                        g.setColor(trisColor[k]);
                        trisPiece[k].translate(i*unit,j*unit);
                        g.fillPolygon(trisPiece[k]);
                        trisPiece[k].translate(-i*unit,-j*unit);
                    }
                }
            }
        }

    }    

    private void generateNewBrick() {
    }



    void oneStepDown() {
    }

    void analyzeRow() {
    }



    public int getNextBrick() {

        return  0;
    }

    public boolean moveLeft() {

        return  true;
    }

    public boolean moveRight() {

        return  true;
    }

    public boolean moveUp() {

        return  true;
    }

    public boolean moveDown() {

        return  true;
    }

    public boolean rotate() {
        if(rotDirection == 1) {

        } else if(rotDirection == 2) {

        } else {

        }

        return  true;
    }


    public void drop() {

    }

    public void pause() {

    }


    public static void main(String[] args) {
        LookFeel.initializeLF();
        
        TrisField  fieldPane = new TrisField();
        JFrame frame = new JFrame();
        frame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });
        frame.getContentPane().add(fieldPane);
        frame.setTitle("TrisField Demo");
        //frame.setSize(450, 350);
        frame.pack();
        frame.setVisible(true);
    }

}

