//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Tris: The Addictive Game! in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Tris: The Addictive Game!
Tris is a Tetr*s-clone game.
Each brick in Tris consists of three triangular pieces.

@copyright  Hyoungsoo Yoon
@date  April 25th, 1999
*/
package com.bluecraft.tris;

import com.bluecraft.tris.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;


/**
TrisFrame is ...

@author  Hyoungsoo Yoon
@version  0.3
*/
public class TrisFrame extends JFrame
    implements ActionListener, ItemListener {
    
    final static boolean DEBUG = false;
    ResourceBundle res = ResourceBundle.getBundle("com.bluecraft.tris.ResBundle");

    protected JTextArea textArea;
    protected String newline = "\n";
    
    private boolean bShowToolBar = true;
    private boolean bToolBarWithText = false;
    private boolean bShowStatusBar = true;
    
    protected Action newFileAction;
    protected Action openFileAction;
    protected Action saveFileAction;
    protected Action exitAction;
    protected Action newGameAction;
    protected Action pauseAction;
    protected Action stopGameAction;
    protected Action aboutAction;
    
    private JLabel scoreLabel = new JLabel();
    private JLabel statusBar = new JLabel();

    
    public TrisFrame() {
        this("Tris: Try Tris!");
    }    

    public TrisFrame(String frameTitle) {
        //Do frame stuff.
        super(frameTitle);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });


        // set default size
        setSize(new Dimension(450, 400));
        setTitle("Tris: Try Tris!");

        //Create the toolbar and menu.
        JToolBar toolBar = new JToolBar();
        JMenu fileMenu = createFileMenu(toolBar);
        JMenu gameMenu = createGameMenu(toolBar);
        JMenu viewMenu = createViewMenu(toolBar);
        JMenu helpMenu = createHelpMenu(toolBar);
        statusBar.setText("Status Bar");

        //Create the text area used for log output.
        textArea = new JTextArea(5, 30);
        JScrollPane logPane = new JScrollPane(textArea);
        
        //Create game field
        JPanel fieldPane = new TrisPanel();

        //Create score board
	scoreLabel.setText(res.getString("scoreLabel"));
        JPanel scorePane = new JPanel();
	scorePane.setLayout(new BoxLayout(scorePane, BoxLayout.Y_AXIS));
	scorePane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
	scorePane.setPreferredSize(new Dimension(100, 200));
	scorePane.setMinimumSize(new Dimension(30, 30));

        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new FlowLayout());
        labelPanel.add(scoreLabel);
        JPanel tipsPanel = new JPanel();
        tipsPanel.setLayout(new FlowLayout());
        tipsPanel.add(new JTextArea(5,30));

        scorePane.add(labelPanel);
	scorePane.add(Box.createRigidArea(new Dimension(0,5)));
        scorePane.add(tipsPanel);

        //Lay out the game pane.
        JPanel gamePane = new JPanel();
        gamePane.setLayout(new BorderLayout());
        gamePane.setPreferredSize(new Dimension(400, 300));
        gamePane.add(fieldPane, BorderLayout.CENTER);
        gamePane.add(scorePane, BorderLayout.WEST);

        //Lay out the main pane.
        JPanel mainPane = new JPanel();
        mainPane.setLayout(new BorderLayout());
        mainPane.setPreferredSize(new Dimension(400, 300));
        mainPane.add(gamePane, BorderLayout.CENTER);
        if(DEBUG) {
            mainPane.add(logPane, BorderLayout.SOUTH);
	}
      
        //Lay out the content pane.
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        //contentPane.setPreferredSize(new Dimension(450, 350));
        contentPane.add(toolBar, BorderLayout.NORTH);
        contentPane.add(mainPane, BorderLayout.CENTER);
        contentPane.add(statusBar, BorderLayout.SOUTH);
        
        // Set content pane
        setContentPane(contentPane);
        

        //Set up the menu bar.
        JMenuBar mb = new JMenuBar();
        mb.add(fileMenu);
        mb.add(gameMenu);
        mb.add(viewMenu);
        mb.add(helpMenu);
        setJMenuBar(mb);
    }

    // file Menu 
    protected JMenu createFileMenu(JToolBar toolBar) {
    
        JMenu fileMenu = new JMenu("File");

        JButton button = null;
        JMenuItem menuItem = null;
        ImageIcon imgIcon = null;

        // New file
        imgIcon = new ImageIcon(com.bluecraft.tris.TrisFrame.class.getResource("images/newFile.gif"));
        newFileAction = new AbstractAction("New", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    displayResult("Action for New File", e);
                }
            };
        button = toolBar.add(newFileAction);
        if(bToolBarWithText) {
            button.setText("New");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("New File");
        menuItem = fileMenu.add(newFileAction);
        menuItem.setIcon(imgIcon);

        // Open file
        imgIcon = new ImageIcon(com.bluecraft.tris.TrisFrame.class.getResource("images/openFile.gif"));
        openFileAction = new AbstractAction("Open...", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    displayResult("Action for Open File", e);
                }
            };
        button = toolBar.add(openFileAction);
        if(bToolBarWithText) {
            button.setText("Open");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Open File");
        menuItem = fileMenu.add(openFileAction);
        menuItem.setIcon(imgIcon);
        
        // Save file
        imgIcon = new ImageIcon(com.bluecraft.tris.TrisFrame.class.getResource("images/closeFile.gif"));
        saveFileAction = new AbstractAction("Save", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    displayResult("Action for Save File", e);
                }
            };
        button = toolBar.add(saveFileAction);
        if(bToolBarWithText) {
            button.setText("Save");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Save File");
        menuItem = fileMenu.add(saveFileAction);
        menuItem.setIcon(imgIcon);
        
        fileMenu.addSeparator();

        // Exit
        imgIcon = new ImageIcon(com.bluecraft.tris.TrisFrame.class.getResource("images/dummy.gif"));
        exitAction = new AbstractAction("Exit", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    System.exit(0);
                    //displayResult("Action for Exit", e);
                }
            };
        menuItem = fileMenu.add(exitAction);
        menuItem.setIcon(null);
        
        return fileMenu;
    }


    // game Menu 
    protected JMenu createGameMenu(JToolBar toolBar) {
    
        JMenu gameMenu = new JMenu("Game");

        JButton button = null;
        JMenuItem menuItem = null;
        ImageIcon imgIcon = null;

        // New Game
        imgIcon = new ImageIcon(com.bluecraft.tris.TrisFrame.class.getResource("images/dummy.gif"));
        newGameAction = new AbstractAction("New Game", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    newGameAction.setEnabled(false);
                    displayResult("Action for New Game", e);
                }
            };
        button = toolBar.add(newGameAction);
        if(bToolBarWithText) {
            button.setText("New Game");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("New Game");
        menuItem = gameMenu.add(newGameAction);
        menuItem.setIcon(imgIcon);

        // Pause/Resume
        imgIcon = new ImageIcon(com.bluecraft.tris.TrisFrame.class.getResource("images/dummy.gif"));
        pauseAction = new AbstractAction("Pause", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    //newGameAction.setEnabled(false);
                    displayResult("Action for Pause", e);
                }
            };
        button = toolBar.add(pauseAction);
        if(bToolBarWithText) {
            button.setText("Pause");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Pause");
        menuItem = gameMenu.add(pauseAction);
        menuItem.setIcon(imgIcon);
        
        // Stop
        imgIcon = new ImageIcon(com.bluecraft.tris.TrisFrame.class.getResource("images/dummy.gif"));
        stopGameAction = new AbstractAction("Stop Game", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    newGameAction.setEnabled(true);
                    displayResult("Action for Stop Game", e);
                }
            };
        button = toolBar.add(stopGameAction);
        if(bToolBarWithText) {
            button.setText("Stop Game");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Stop Game");
        menuItem = gameMenu.add(stopGameAction);
        menuItem.setIcon(imgIcon);
        
        return gameMenu;
    }

    // LookandFeel Menu
    protected JMenu createLookFeelMenu() {
        JMenu lfMenu = new JMenu("Look/Feel");
        
        //a group of radio button menu items
        ButtonGroup group = new ButtonGroup();
        JRadioButtonMenuItem rbMenuItem = null;
        
        rbMenuItem = new JRadioButtonMenuItem(LookFeel.METAL_LF_NAME);
        rbMenuItem.setSelected(true);  // Meta LF is the default
        group.add(rbMenuItem);
        rbMenuItem.addActionListener(this);
        rbMenuItem.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    JRadioButtonMenuItem mi = (JRadioButtonMenuItem)(e.getSource());
                    boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                    if(selected) {
                        LookFeel.changeLF(TrisFrame.this, LookFeel.METAL_LF);
                    }    
                }
            });
        lfMenu.add(rbMenuItem);
        
        lfMenu.addSeparator();
        
        rbMenuItem = new JRadioButtonMenuItem(LookFeel.WINDOWS_LF_NAME);
        rbMenuItem.setSelected(false);
        group.add(rbMenuItem);
        rbMenuItem.addActionListener(this);
        rbMenuItem.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    JRadioButtonMenuItem mi = (JRadioButtonMenuItem)(e.getSource());
                    boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                    if(selected) {
                        LookFeel.changeLF(TrisFrame.this, LookFeel.WINDOWS_LF);
                    }    
                }
            });
        lfMenu.add(rbMenuItem);
        
        rbMenuItem = new JRadioButtonMenuItem(LookFeel.MOTIF_LF_NAME);
        rbMenuItem.setSelected(false);
        group.add(rbMenuItem);
        rbMenuItem.addActionListener(this);
        rbMenuItem.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    JRadioButtonMenuItem mi = (JRadioButtonMenuItem)(e.getSource());
                    boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                    if(selected) {
                        LookFeel.changeLF(TrisFrame.this, LookFeel.MOTIF_LF);
                    }    
                }
            });
        lfMenu.add(rbMenuItem);
        
        // Mac Look and Feel not available!
        rbMenuItem = new JRadioButtonMenuItem(LookFeel.MAC_LF_NAME);
        rbMenuItem.setSelected(false);
        rbMenuItem.setEnabled(false);
        group.add(rbMenuItem);
        rbMenuItem.addActionListener(this);
        rbMenuItem.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    JRadioButtonMenuItem mi = (JRadioButtonMenuItem)(e.getSource());
                    boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                    if(selected) {
                        LookFeel.changeLF(TrisFrame.this, LookFeel.MAC_LF);
                    }    
                }
            });
        lfMenu.add(rbMenuItem);

        return lfMenu;
    }


    // View Menu 
    protected JMenu createViewMenu(JToolBar toolBar) {
    
        JMenu vMenu = new JMenu("View");
	
        JCheckBoxMenuItem cbMenuItem = null;

	// Toggle ToolBar
        cbMenuItem = new JCheckBoxMenuItem("Toggle ToolBar");
        cbMenuItem.setSelected(bShowToolBar);
        //cbMenuItem.setEnabled(true);
        cbMenuItem.addActionListener(this);
        cbMenuItem.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    JCheckBoxMenuItem mi = (JCheckBoxMenuItem)(e.getSource());
                    boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                    if(selected) {
                        bShowToolBar = true;
                    } else {
                        bShowToolBar = false;
                    }
                }
            });
        vMenu.add(cbMenuItem);

	// Toggle StatusBar
        cbMenuItem = new JCheckBoxMenuItem("Toggle StatusBar");
        cbMenuItem.setSelected(bShowStatusBar);
        //cbMenuItem.setEnabled(true);
        cbMenuItem.addActionListener(this);
        cbMenuItem.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    JCheckBoxMenuItem mi = (JCheckBoxMenuItem)(e.getSource());
                    boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                    if(selected) {
                        bShowStatusBar = true;
                    } else {
                        bShowStatusBar = false;
                    }
                }
            });
        vMenu.add(cbMenuItem);

        vMenu.addSeparator();

	JMenu lookFeelMenu = createLookFeelMenu();
	vMenu.add(lookFeelMenu);
        
        return vMenu;
    }


    // help Menu 
    protected JMenu createHelpMenu(JToolBar toolBar) {
    
        JMenu helpMenu = new JMenu("Help");

        JButton button = null;
        JMenuItem menuItem = null;
        ImageIcon imgIcon = null;

        // About_Box Dialog
        imgIcon = new ImageIcon(com.bluecraft.tris.TrisFrame.class.getResource("images/help.gif"));
        aboutAction = new AbstractAction("About", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    //displayResult("Action for AboutBox", e);
                    helpAbout_actionPerformed(e);
                }
            };
        button = toolBar.add(aboutAction);
        if(bToolBarWithText) {
            button.setText("About");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("About Tris");
        menuItem = helpMenu.add(aboutAction);
        menuItem.setIcon(imgIcon);
        
        return helpMenu;
    }



    protected void displayResult(String actionDescription, ActionEvent e) {
        String s = ("Action event detected by: " + actionDescription
                    + newline 
                    + "    Event source: " + e.getSource()
                    + newline);
        textArea.append(s);
    }


    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        String s = "Action event detected."
            + newline
            + "    Event source: " + source.getText()
            + " (an instance of " + getClassName(source) + ")";
        textArea.append(s + newline);
    }

    public void itemStateChanged(ItemEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        String s = "Item event detected."
            + newline
            + "    Event source: " + source.getText()
            + " (an instance of " + getClassName(source) + ")"
            + newline
            + "    New state: " 
            + ((e.getStateChange() == ItemEvent.SELECTED) ?
               "selected":"unselected");
        textArea.append(s + newline);
    }

    // Returns just the class name -- no package info.
    protected String getClassName(Object o) {
        String classString = o.getClass().getName();
        int dotIndex = classString.lastIndexOf(".");
        return classString.substring(dotIndex+1);
    }



    //Help | About action performed
    public void helpAbout_actionPerformed(ActionEvent e) {
        TrisFrame_AboutBox dlg = new TrisFrame_AboutBox(this);
        Dimension dlgSize = dlg.getPreferredSize();
        Dimension frmSize = getSize();
        Point loc = getLocation();
        dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
        dlg.setModal(true);
        dlg.show();
    }



    public static void main(String[] args) {
        LookFeel.initializeLF();
        
        TrisFrame frame = new TrisFrame();
        //frame.setTitle("TrisFrame Demo");
        //frame.setSize(450, 350);
        frame.pack();
        frame.setVisible(true);

        // ???
        //frame.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }

}




