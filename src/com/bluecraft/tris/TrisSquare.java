//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Tris: The Addictive Game! in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Tris: The Addictive Game!
Tris is a Tetr*s-clone game.
Each brick in Tris consists of three triangular pieces.

@copyright  Hyoungsoo Yoon
@date  April 25th, 1999
*/
package com.bluecraft.tris;

import java.awt.*;


/**
TrisSquare is ...

@author  Hyoungsoo Yoon
@version  0.3
*/
public interface TrisSquare {
    final static int unit = 20;  // grid size in pixel
    final static int sqSize = unit - 2;

    final static int[] trisMask = {0,1,2,4,8};
    final static String[] trisName = {"NONE", "NW", "NE", "SE", "SW"};
    final static Color[] trisColor = {Color.white,
                                      Color.blue,
                                      Color.red,
                                      Color.green,
                                      Color.yellow};
    final static Polygon[] trisPiece =
    {new Polygon(new int[]{1,sqSize,sqSize,1},new int[]{1,1,sqSize,sqSize},4),
     new Polygon(new int[]{0,unit,0},new int[]{0,0,unit},3),
     new Polygon(new int[]{0,unit,unit},new int[]{0,0,unit},3),
     new Polygon(new int[]{unit,unit,0},new int[]{0,unit,unit},3),
     new Polygon(new int[]{0,unit,0},new int[]{0,unit,unit},3)};
}

