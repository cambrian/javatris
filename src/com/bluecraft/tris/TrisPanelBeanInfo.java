//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Tris: The Addictive Game! in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Tris: The Addictive Game!
Tris is a Tetr*s-clone game.
Each brick in Tris consists of three triangular pieces.

@copyright  Hyoungsoo Yoon
@date  April 25th, 1999
*/
package com.bluecraft.tris;

import java.beans.*;

/**
TrisPanelBeanInfo is ...

@author  Hyoungsoo Yoon
@version  0.3
*/
public class TrisPanelBeanInfo extends SimpleBeanInfo {
    Class beanClass = TrisPanel.class;
    String iconColor16x16Filename = "images/TrisPanel16x16.gif";
    String iconColor32x32Filename = "images/TrisPanel32x32.gif";
    String iconMono16x16Filename;
    String iconMono32x32Filename;

  
    public TrisPanelBeanInfo() {
    }

    public PropertyDescriptor[] getPropertyDescriptors() {
        try  {
            PropertyDescriptor _sample = new PropertyDescriptor("sample", beanClass, "getSample", "setSample");
      
            PropertyDescriptor[] pds = new PropertyDescriptor[] {
                _sample,
            };
            return pds;
        }
        catch (IntrospectionException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public java.awt.Image getIcon(int iconKind) {
        switch (iconKind) {
        case BeanInfo.ICON_COLOR_16x16:
            return iconColor16x16Filename != null ? loadImage(iconColor16x16Filename) : null;
        case BeanInfo.ICON_COLOR_32x32:
            return iconColor32x32Filename != null ? loadImage(iconColor32x32Filename) : null;
        case BeanInfo.ICON_MONO_16x16:
            return iconMono16x16Filename != null ? loadImage(iconMono16x16Filename) : null;
        case BeanInfo.ICON_MONO_32x32:
            return iconMono32x32Filename != null ? loadImage(iconMono32x32Filename) : null;
        }
        return null;
    }
}


