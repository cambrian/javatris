//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Tris: The Addictive Game! in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Tris: The Addictive Game!
Tris is a Tetr*s-clone game.
Each brick in Tris consists of three triangular pieces.

@copyright  Hyoungsoo Yoon
@date  April 25th, 1999
*/
package com.bluecraft.tris;


/**
TrisBlock is a four clustered squares.

@author  Hyoungsoo Yoon
@version  0.3
*/
public class TrisBlock {
    int[]  occupied = {0, 0, 0, 0};  // NW, NE, SE, SW
    // 0: empty, 1:nw, 2:ne, 4:se, 8:sw 

    public TrisBlock() {
        // Empty block
        occupied[0] = 0;
        occupied[1] = 0;
        occupied[2] = 0;
        occupied[3] = 0;
    }

    public TrisBlock(int i, int j, int k, int l) {
        // Specify occupancy
        occupied[0] = i;
        occupied[1] = j;
        occupied[2] = k;
        occupied[3] = l;
    }

    public TrisBlock(TrisBlock source) {
        // Specify occupancy
        occupied[0] = source.occupied[0];
        occupied[1] = source.occupied[1];
        occupied[2] = source.occupied[2];
        occupied[3] = source.occupied[3];
    }

    private int rotatePieceCW(int orient) {
        switch(orient) {
        case 0:
        default:
            return 0;
        case 1:
            return 2;
        case 2:
            return 4;
        case 4:
            return 8;
        case 8:
            return 1;
	}
    }

    private int rotatePieceCCW(int orient) {
        switch(orient) {
        case 0:
        default:
            return 0;
        case 1:
            return 8;
        case 2:
            return 1;
        case 4:
            return 2;
        case 8:
            return 4;
	}
    }


    public boolean rotateCW() {
        TrisBlock  tmpCopy = new TrisBlock(this);
        occupied[0] = rotatePieceCW(tmpCopy.occupied[3]);
        occupied[1] = rotatePieceCW(tmpCopy.occupied[0]);
        occupied[2] = rotatePieceCW(tmpCopy.occupied[1]);
        occupied[3] = rotatePieceCW(tmpCopy.occupied[2]);

        return  true;
    }

    public boolean rotateCCW() {
        TrisBlock  tmpCopy = new TrisBlock(this);
        occupied[0] = rotatePieceCCW(tmpCopy.occupied[1]);
        occupied[1] = rotatePieceCCW(tmpCopy.occupied[2]);
        occupied[2] = rotatePieceCCW(tmpCopy.occupied[3]);
        occupied[3] = rotatePieceCCW(tmpCopy.occupied[0]);

        return  true;
    }

}    
