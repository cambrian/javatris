//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Tris: The Addictive Game! in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Tris: The Addictive Game!
Tris is a Tetr*s-clone game.
Each brick in Tris consists of three triangular pieces.

@copyright  Hyoungsoo Yoon
@date  April 25th, 1999
*/
package com.bluecraft.tris;


/**
TrisBrick is ...

@author  Hyoungsoo Yoon
@version  0.3
*/
public class TrisBrick implements TrisBrickList {


    public boolean moveLeft() {

        return  true;
    }

    public boolean moveRight() {

        return  true;
    }

    public boolean moveUp() {

        return  true;
    }

    public boolean moveDown() {

        return  true;
    }

    public boolean rotateCW() {

        return  true;
    }

    public boolean rotateCCW() {

        return  true;
    }

    public void drop() {

    }

}    
