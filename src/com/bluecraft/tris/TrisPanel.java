//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Tris: The Addictive Game! in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Tris: The Addictive Game!
Tris is a Tetr*s-clone game.
Each brick in Tris consists of three triangular pieces.

@copyright  Hyoungsoo Yoon
@date  April 25th, 1999
*/
package com.bluecraft.tris;

import com.bluecraft.tris.util.*;
import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.awt.event.*;
import java.util.*;
import java.lang.Runnable;


/**
TrisPanel is ...

@author  Hyoungsoo Yoon
@version  0.3
*/
public class TrisPanel extends JPanel 
    implements Serializable, KeyListener, Runnable {
  
    ResourceBundle res = ResourceBundle.getBundle("com.bluecraft.tris.ResBundle");

    private String sample = res.getString("Sample");

    private JPanel fieldPanel = new TrisField();
    private JPanel sidePanel = new JPanel();
    private JPanel previewPanel = new JPanel();

    JLabel statsLabel1 = new JLabel();
    JLabel statsLabel2 = new JLabel();

    private transient Vector keyListeners;

  
    public TrisPanel() {
        try  {
            setLayout(new BorderLayout());

            statsLabel1.setText(res.getString("statsLabel1"));
            statsLabel2.setText(res.getString("statsLabel2"));
            previewPanel.add(new JTextArea(5,5));

            sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.Y_AXIS));
            sidePanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
            sidePanel.add(previewPanel);
            sidePanel.add(Box.createRigidArea(new Dimension(0,5)));

            JPanel statsPanel = new JPanel();
            statsPanel.setLayout(new GridLayout(20,1,2,2));
            statsPanel.add(statsLabel1);
            statsPanel.add(statsLabel2);
            sidePanel.add(statsPanel);

            fieldPanel.setPreferredSize(new Dimension(100, 200));
            fieldPanel.setMinimumSize(new Dimension(30, 30));

            this.add(fieldPanel, BorderLayout.CENTER);
            this.add(sidePanel, BorderLayout.EAST);

            this.addKeyListener(new java.awt.event.KeyAdapter() {
                    public void keyPressed(KeyEvent e) {
                        this_keyPressed(e);
                    }
                });
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public String getSample() {
        return sample;
    }

    public void setSample(String newSample) {
        sample = newSample;
    }


    public static void main(String[] args) {
        TrisPanel trisPanel1 = new TrisPanel();
    }


    void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
    }

    void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
        ois.defaultReadObject();
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    public synchronized void removeKeyListener(KeyListener l) {
        super.removeKeyListener(l);
        if (keyListeners != null && keyListeners.contains(l)) {
            Vector v = (Vector) keyListeners.clone();
            v.removeElement(l);
            keyListeners = v;
        }
    }

    public synchronized void addKeyListener(KeyListener l) {
        super.addKeyListener(l);
        Vector v = keyListeners == null ? new Vector(2) : (Vector) keyListeners.clone();
        if (!v.contains(l)) {
            v.addElement(l);
            keyListeners = v;
        }
    }

    public void run() {
        //TODO: implement this java.lang.Runnable method;

    }

    void this_keyPressed(KeyEvent e) {

    }

}



