//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Tris: The Addictive Game! in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Tris: The Addictive Game!
Tris is a Tetr*s-clone game.
Each brick in Tris consists of three triangular pieces.

@copyright  Hyoungsoo Yoon
@date  April 25th, 1999
*/
package com.bluecraft.tris;

import com.bluecraft.tris.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;


/**
TrisFrame_AboutBox is ...

@author  Hyoungsoo Yoon
@version  0.3
*/
public class TrisFrame_AboutBox extends Dialog implements ActionListener {

    JPanel panel1 = new JPanel();
    JPanel panel2 = new JPanel();
    JPanel insetsPanel1 = new JPanel();
    JPanel insetsPanel2 = new JPanel();
    JPanel insetsPanel3 = new JPanel();
    JButton button1 = new JButton();
    JLabel imageControl1 = new JLabel();
    ImageIcon imageIcon;
    JLabel label1 = new JLabel();
    JLabel label2 = new JLabel();
    JLabel label3 = new JLabel();
    JLabel label4 = new JLabel();
    BorderLayout borderLayout1 = new BorderLayout();
    BorderLayout borderLayout2 = new BorderLayout();
    FlowLayout flowLayout1 = new FlowLayout();
    FlowLayout flowLayout2 = new FlowLayout();
    GridLayout gridLayout1 = new GridLayout();
    String product = "Tris: The Addictive Game";
    String version = "";
    String copyright = "Copyright (c) 1998";
    String comments = "Tris is a Tetris-clone game.";
  
    public TrisFrame_AboutBox(Frame parent) {
        super(parent);
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        try  {
            //imageIcon = new ImageIcon(getClass().getResource("your image name goes here"));
            this.setTitle("About");
            setResizable(false);
            panel1.setLayout(borderLayout1);
            panel2.setLayout(borderLayout2);
            insetsPanel1.setLayout(flowLayout1);
            insetsPanel2.setLayout(flowLayout1);
            insetsPanel2.setBorder(new EmptyBorder(10, 10, 10, 10));
            gridLayout1.setRows(4);
            gridLayout1.setColumns(1);
            label1.setText(product);
            label2.setText(version);
            label3.setText(copyright);
            label4.setText(comments);
            insetsPanel3.setLayout(gridLayout1);
            insetsPanel3.setBorder(new EmptyBorder(10, 60, 10, 10));
            button1.setText("OK");
            button1.addActionListener(this);
            insetsPanel2.add(imageControl1, null);
            panel2.add(insetsPanel2, BorderLayout.WEST);
            this.add(panel1, null);
            insetsPanel3.add(label1, null);
            insetsPanel3.add(label2, null);
            insetsPanel3.add(label3, null);
            insetsPanel3.add(label4, null);
            panel2.add(insetsPanel3, BorderLayout.CENTER);
            insetsPanel1.add(button1, null);
            panel1.add(insetsPanel1, BorderLayout.SOUTH);
            panel1.add(panel2, BorderLayout.NORTH);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        //imageControl1.setIcon(imageIcon);
        pack();
    }


    protected void processWindowEvent(WindowEvent e) {
        if (e.getID() == WindowEvent.WINDOW_CLOSING) {
            cancel();
        }
        super.processWindowEvent(e);
    }

    void cancel() {
        dispose();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == button1) {
            cancel();
        }
    }


}



