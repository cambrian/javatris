//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Tris: The Addictive Game! in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Tris: The Addictive Game!
Tris is a Tetr*s-clone game.
Each brick in Tris consists of three triangular pieces.

@copyright  Hyoungsoo Yoon
@date  April 25th, 1999
*/
package com.bluecraft.tris;

import com.bluecraft.tris.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.applet.*;
import java.lang.Runnable;


/**
TrisApplet is ...

@author  Hyoungsoo Yoon
@version  0.3
*/
public class TrisApplet extends JApplet implements Runnable {
    boolean isStandalone = false;
    int width;
    int height;

    //Get a parameter value
    public String getParameter(String key, String def) {
        return isStandalone ? System.getProperty(key, def) :
            (getParameter(key) != null ? getParameter(key) : def);
    }

    //Construct the applet
    public TrisApplet() {
    }

    //Initialize the applet
    public void init() {
        try { width = Integer.parseInt(this.getParameter("Width", "200")); } catch (Exception e) { e.printStackTrace(); }
        try { height = Integer.parseInt(this.getParameter("Height", "300")); } catch (Exception e) { e.printStackTrace(); }
        try {
            this.setSize(400,300);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    static { 
        try { 
            LookFeel.initializeLF();
        }
        catch (Exception e) {}
    }


    //Start the applet
    public void start() {
    }

    //Stop the applet
    public void stop() {
    }

    //Destroy the applet
    public void destroy() {
    }

    //Get Applet information
    public String getAppletInfo() {
        return "Applet Information";
    }

    //Get parameter info
    public String[][] getParameterInfo() {
        String pinfo[][] = 
        {
            {"Width", "int", ""},
            {"Height", "int", ""},
        };
        return pinfo;
    }

    //Main method
    public static void main(String[] args) {
        TrisApplet applet = new TrisApplet();
        applet.isStandalone = true;
        JFrame frame = new JFrame();
        frame.setTitle("Applet Frame");
        frame.getContentPane().add(applet, BorderLayout.CENTER);
        applet.init();
        applet.start();
        frame.setSize(400,320);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation((d.width - frame.getSize().width) / 2, (d.height - frame.getSize().height) / 2);
        frame.setVisible(true);
    }

    public void run() {
        //TODO: implement this java.lang.Runnable method;
    }
}



