//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Tris: The Addictive Game! in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Tris: The Addictive Game!
Tris is a Tetr*s-clone game.
Each brick in Tris consists of three triangular pieces.

@copyright  Hyoungsoo Yoon
@date  April 25th, 1999
*/
package com.bluecraft.tris;


/**
TrisBrickList is ...

@author  Hyoungsoo Yoon
@version  0.3
*/
public interface TrisBrickList {
    final static TrisBlock[] brick = {new TrisBlock(0,0,0,0),
                                      new TrisBlock(4,8,0,2),
                                      new TrisBlock(4,8,0,1),
                                      new TrisBlock(4,1,0,2),
                                      new TrisBlock(4,1,0,1),
                                      new TrisBlock(4,5,0,0),
                                      new TrisBlock(4,0,0,5),
                                      new TrisBlock(10,1,0,0),
                                      new TrisBlock(10,0,0,1)};
}
