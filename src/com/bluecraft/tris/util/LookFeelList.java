//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Tris: The Addictive Game! in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Tris: The Addictive Game!
Tris is a Tetris-clone game.
Each brick in Tris consists of three triangular pieces.

@copyright  Hyoungsoo Yoon
@date  April 5th, 1999
*/
package com.bluecraft.tris.util;


/**
LookFeelList defines the name of available LookAndFeels.
This interface is implemented by LookFeel, which is a helper class 
to easily set and manipulate LookAndFeel of Java classes.

@author  Hyoungsoo Yoon
@version  0.3
*/
public interface LookFeelList {
    final static String SYSTEM_LF= "System";
    final static String METAL_LF= "Metal";
    final static String METAL_LF_NAME= "Metal Look and Feel";
    final static String METAL_LF_CLASS = "javax.swing.plaf.metal.MetalLookAndFeel";
    final static String MOTIF_LF = "Motif";
    final static String MOTIF_LF_NAME = "Motif Look and Feel";
    final static String MOTIF_LF_CLASS = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
    final static String WINDOWS_LF = "Windows";
    final static String WINDOWS_LF_NAME = "Windows Look and Feel";
    final static String WINDOWS_LF_CLASS = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
    final static String MAC_LF = "Mac";
    final static String MAC_LF_NAME = "Mac Look and Feel";
    final static String MAC_LF_CLASS = "com.sun.java.swing.plaf.mac.macLookAndFeel";
}    
